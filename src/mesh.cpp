#define STB_IMAGE_IMPLEMENTATION
#include "Mesh.hpp"

Mesh::Mesh(const std::vector<Vertex> vertices, const std::vector<GLuint> indices)
{
    this->indices_size = indices.size();
    setup(vertices, indices);
}
void Mesh::Draw()
{
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, (int)this->indices_size, GL_UNSIGNED_INT, 0);

}
void Mesh::setup(std::vector<Vertex> vertices, std::vector<GLuint> indices)
{
    glGenBuffers(2, VIBO);
    glGenVertexArrays(1, &VAO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VIBO[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VIBO[1]);

    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_size * sizeof(int), &indices[0], GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Texel));

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
}
Mesh::~Mesh() {}



GLuint loadTexture(const char* filename)
{
    GLuint textureID;
    glGenTextures(1, &textureID);

    int width, height, channels;
    GLubyte *data = stbi_load(filename, &width, &height, &channels, 0);
    if (data)
    {
        GLenum format = GL_RED;
        switch (channels)
        {
        case 3:
            format = GL_RGB;
            break;
        case 4:
            format = GL_RGBA;
            break;
        }
        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << filename << std::endl;
    }
    stbi_image_free(data);
    return textureID;
}
