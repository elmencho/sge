#include "Model.hpp"
#include <glad/glad.h>
using namespace glm;
using namespace Assimp;

Model::Model(std::string objPath)
{
    path = objPath.substr(0, objPath.find_last_of('/'));

    Importer importer;
    const aiScene* scene = importer.ReadFile(objPath, aiProcess_Triangulate
                             | aiProcess_JoinIdenticalVertices);

    std::cout << importer.GetErrorString();
    getMeshes(scene->mRootNode, scene);
}
void Model::Draw()
{
    for (Mesh& mesh : meshes)
    {
        mesh.Draw();
    }
}
Model::~Model()
{
    for (Texture texture : texture_cache)
    {
        glDeleteTextures(1, &texture.id);
    }
    for (Mesh& mesh : meshes)
    {
        glDeleteBuffers(2, mesh.VIBO);
        glDeleteVertexArrays(1, &mesh.VAO);
    }
}
void Model::getMeshes(aiNode* node, const aiScene* scene)
{
    meshes.reserve(node->mNumMeshes);
    for (GLuint i = 0; i < node->mNumMeshes; i++)
    {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        meshes.push_back(createMesh(mesh, scene));
    }

    for (GLuint j = 0; j < node->mNumChildren; j++)
    {
        getMeshes(node->mChildren[j], scene);
    }
}
Mesh Model::createMesh(aiMesh* mesh, const aiScene* scene)
{
    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;
    vertices.reserve(mesh->mNumVertices);
    size_t num_indices = 0;

    for (GLuint i = 0; i < mesh->mNumVertices; i++)
    {
        Vertex vertex = {};
        vertex.Position = vec3(
                              mesh->mVertices[i].x,
                              mesh->mVertices[i].y,
                              mesh->mVertices[i].z
                          );
        vertex.Normal = vec3(
                            mesh->mNormals[i].x,
                            mesh->mNormals[i].y,
                            mesh->mNormals[i].z
                        );
        vertex.Texel = vec2(
                           mesh->mTextureCoords[0][i].x,
                           mesh->mTextureCoords[0][i].y
                       );
        vertices.push_back(vertex);
    }


    for (GLuint i = 0; i < mesh->mNumFaces; i++)
    {
        num_indices += mesh->mFaces[i].mNumIndices;
    }

    indices.reserve(num_indices);

    for (GLuint i = 0; i < mesh->mNumFaces; i++)
    {
        for (GLubyte j = 0; j < mesh->mFaces[i].mNumIndices; j++)
            indices.push_back(mesh->mFaces[i].mIndices[j]);
    }


    aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
    loadTextures(material, aiTextureType_DIFFUSE, "diffuse");
    loadTextures(material, aiTextureType_SPECULAR, "specular");
    loadTextures(material, aiTextureType_NORMALS, "normals");

    return Mesh(vertices, indices);
}
void Model::loadTextures(aiMaterial* material, const aiTextureType aitype, const char* type)
{
    for (GLubyte i = 0; i < material->GetTextureCount(aitype); i++)
    {
        aiString filename;
        material->GetTexture(aitype, i, &filename);

        bool skip = false;
        for (Texture texture : texture_cache)
        {
            if (std::string(filename.C_Str()) == texture.name)
            {
                skip = true;
                break;
            }
        }

        if (!skip)
        {
            Texture texture;
            glActiveTexture(GL_TEXTURE0 + (GLenum)texture_cache.size());
            texture.id = loadTexture((path + "/" + filename.C_Str()).c_str());
            texture.type = type;
            texture.name = std::string(filename.C_Str());
            texture_cache.push_back(texture);
        }
    }
}
