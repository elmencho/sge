#include "Window.hpp"
#include "Engine.hpp"
#include <GLFW/glfw3.h>
#include <glm/fwd.hpp>
using namespace glm;


void mouse_callback(GLFWwindow, double, double);
Engine engine(initWindow(GLFW));


int main()
{
    initGLES();
    engine.init();
    engine.addLight("models/cube.obj", Engine::light_point);
    //engine.addTerrain("models/cube.obj", "shaders/floor.frag");
    engine.addItem("models/backpack/backpack.obj");
    engine.addItem("models/cube.obj");

    //engine.changeItem(0, Engine::NEW, vec3(0.0f), vec3(64.0f, 0.001f, 64.0f));
    engine.changeItem(0, Engine::NEW, vec3(0.0f, 2.0f, 0.0f));
    engine.addMark(1);

    engine.addStack(1, 3LL);

    const float& time = engine.Frame;
    while (!glfwWindowShouldClose(engine.camera->window))
    {

        for (int i = 1; i < 4; i++)
        {
            vec3 pos = vec3(cos(time) * 3 * i, i, sin(time) * 3 * i);
            mat4 model = translate(mat4(1.0), pos);
            model = rotate(model, radians(30.0f), vec3(0.0, 1.0, 0.0));
            engine.chgStack(1, i-1, model, pos);
        }
 
        engine.changeLight(0, Engine::NEW, vec3(cos(time) * 20, 6.0f, sin(time) * 20) );
        engine.draw();
        glfwSwapBuffers(engine.camera->window);
        glfwPollEvents();

    }
    glfwTerminate();
    std::cout << glGetError();
}
void mouse_callback(GLFWwindow *window, double x, double y)
{
    engine.camera->rotate_callback(window, (float)x, (float)y);
}
