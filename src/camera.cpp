#include "glad/glad.h"
#include "Camera.hpp"
#include "Window.hpp"
using namespace glm;


Camera::Camera(GLFWwindow* window, vec3 Position, vec3 Front, float fov, float sens)
{
    this->sens  = sens;
    this->Front = Front;
    this->Pos = Position;
    this->window = window;
    this->projection = perspective(fov, 4.0f/3.0f, 0.1f, 100.0f);

    lastx = lasty = 0;
}

void Camera::rotate_callback(GLFWwindow* window, float x, float y)
{
    if (firstCursorAppear)
    {
        lastx = x, lasty = y;
        firstCursorAppear = false;
    }

    yaw += (x-lastx)*sens;
    pitch += (lasty-y)*sens;
    lastx = x, lasty = y;

    pitch > 89.0f ? pitch = 89.0f : true;
    pitch < -89.0f ? pitch = -89.0f : true;
    //if (pitch >  89.0f)		pitch = 89.0f;
    //if (pitch < -89.0f)		pitch = -89.0f;

    Front = normalize(vec3(cos(radians(yaw)) * cos(radians(pitch)),
                           sin(radians(pitch)),
                           sin(radians(yaw)) * cos(radians(pitch))));
}

void Camera::move(float speed)
{
    if (glfwGetKey(window, GLFW_KEY_W))
        this->Pos += this->Front * speed;
    if (glfwGetKey(window, GLFW_KEY_S))
        this->Pos -= this->Front * speed;
    if (glfwGetKey(window, GLFW_KEY_A))
        this->Pos -= normalize(cross(this->Front, this->globalUp)) * speed;
    if (glfwGetKey(window, GLFW_KEY_D))
        this->Pos += normalize(cross(this->Front, this->globalUp)) * speed;

    if (glfwGetKey(window, GLFW_KEY_SPACE))
        this->Pos.y += speed;
    if (glfwGetKey(window, GLFW_KEY_C))
        this->Pos.y -= speed;

    if (glfwGetKey(window, GLFW_KEY_ESCAPE) && !this->firstCursorAppear)
    {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        glfwSetCursorPosCallback(window, 0);
        this->firstCursorAppear = true;
        this->cursorEnabled = false;
    }
    /*else if (!this->cursorEnabled)
    {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        glfwSetCursorPosCallback(window, mouse_callback);
        this->firstCursorAppear = false;
        this->cursorEnabled = true;
    }*/

    this->view = lookAt(this->Pos, this->Pos + this->Front, this->globalUp);
}
