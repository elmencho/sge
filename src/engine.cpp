#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include "Engine.hpp"
using namespace glm;


Engine::Engine(GLFWwindow* window)
{
	this->items = {};
	this->lights = {};
	this->terrain = new Manager::Object;
	this->markShader = nullptr;
	this->marked_items = {};
	Frame = lastFrame = deltaFrame = 0.0f;
	this->camera = new Camera(window, vec3(2.0f, 7.0f, 17.0f), vec3(0.0f, 0.0f, -1.0f), radians(50.0f), 0.05f);
}
void Engine::init()
{
	this->coreShader = new Shader(DEFAULT_VERT, DEFAULT_FRAG);
	this->markShader = new Shader(DEFAULT_VERT, DEFAULT_FRAG_MARK);
	this->coreLampShader = new Shader(DEFAULT_VERT, DEFAULT_FRAG_LIGHT);
}
void Engine::draw()
{
	Frame = (float)glfwGetTime();
	glStencilMask(GL_TRUE);		// write on
	glEnable(GL_DEPTH_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE); /*after successful depth and stencil testing write second arg of StencilFunc to Stencil buffer*/
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glStencilMask(GL_FALSE);	// write off


	glStencilFunc(GL_ALWAYS, 1, GL_TRUE);
	glStencilMask(0xFF);


	coreLampShader->use();
	for (Manager::Object* light : this->lights)
	{
		Manager::setUniform(light, coreLampShader);
		Manager::draw(light);
	}

	coreShader->use();
	for (Item* item : this->items)
	{
		if (item->stack.size())
		{
			for (Manager::UniformWC& uniform : item->stack)
			{
				for (GLubyte i = 0; i < item->object->model->texture_cache.size(); i++)
				{
					glActiveTexture(GL_TEXTURE0+i);
					glBindTexture(GL_TEXTURE_2D, item->object->model->texture_cache[i].id);
				}
				Manager::addUniform(item->object, uniform);
				Manager::setUniform(item->object, coreShader);
				Manager::draw(item->object);
			}
		}
		else
		{
			for (GLubyte i = 0; i < item->object->model->texture_cache.size(); i++)
			{
				glActiveTexture(GL_TEXTURE0+i);
				glBindTexture(GL_TEXTURE_2D, item->object->model->texture_cache[i].id);
			}
			Manager::setUniform(item->object, coreShader);
			Manager::draw(item->object);
		}
	};


	glStencilFunc(GL_NOTEQUAL, 1, GL_TRUE);
	glStencilMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);


	markShader->use();
	for (Item* item : this->marked_items)
	{
		if (item->stack.size())
		{
			for (Manager::UniformWC& uniform : item->stack)
			{
				uniform.model = scale(uniform.model, vec3(1.5f));
				Manager::addUniform(item->object, uniform);
				Manager::setUniform(item->object, markShader);
				Manager::drawMarked(item->object);
			}
		}
		else
		{
			Manager::setUniform(item->object, markShader);
			Manager::drawMarked(item->object);
		}
	}

	deltaFrame = Frame - lastFrame;
	camera->move(6.0f*deltaFrame);
	lastFrame = Frame;
}
void Engine::addItem(std::string objPath)
{
	Item* item = new Item;
	item->object = Manager::loadObject(PREFIX + objPath, coreShader);

	Manager::Uniform uniform;
	uniform.camera = camera;
	uniform.model = mat4(1.0f);
	uniform.position = vec3(1.0f);
	item->object->light = lights[0];

	Manager::addUniform(item->object, uniform);
	items.push_back(item);
}
void Engine::changeItem(GLushort index, Engine::OptionChange opt, vec3 position, vec3 scaling, vec3 rotate_dir, GLfloat rotate_angle)
{
	mat4 model = mat4(1.0f);
	switch (opt)
	{
	case Engine::OWN:
		model = rotate(scale(translate(
			items[index]->object->propert.model,
			position), scaling), radians(rotate_angle),
			rotate_dir);
		break;
	case Engine::NEW:
		model = rotate(scale(translate(
			mat4(1.0f), position), scaling),
			radians(rotate_angle), rotate_dir);
		break;
	}
	items[index]->object->propert.position = position;
	items[index]->object->propert.model = model;
}



void Engine::addLight(std::string mdlPath, LightType type)
{
	Shader* shader = nullptr;
	Manager::Object* object = new Manager::Object;

	Manager::Uniform uniform;
	uniform.camera = camera;
	uniform.model = glm::mat4(1.0f);
	uniform.position = glm::vec3(1.0f);

	switch (type)
	{
	case light_point:
		shader = coreLampShader;
		break;
	}

	object = Manager::loadObject(PREFIX + mdlPath, shader);
	Manager::addUniform(object, uniform);
	lights.push_back(object);
}
void Engine::changeLight(GLushort index, Engine::OptionChange opt, vec3 position, vec3 scaling, vec3 rotate_dir, GLfloat rotate_angle)
{
	mat4 model = mat4(1.0f);
	switch (opt)
	{
	case Engine::OWN:
		model = rotate(scale(translate(
								lights[index]->propert.model, position),
						scaling),
				radians(rotate_angle), rotate_dir);
		break;
	case Engine::NEW:
		model = rotate(scale(translate(
								mat4(1.0f), position),
						scaling),
				radians(rotate_angle), rotate_dir);
		break;
	}
	lights[index]->propert.position = position;
	lights[index]->propert.model = model;
}



void Engine::addStack(GLushort i, mat4 model, vec3 pos)
{
	this->items[i]->stack.push_back({ model, pos });
}
void Engine::addStack(GLushort i, size_t j)
{
	this->items[i]->stack.resize(j);
}
void Engine::chgStack(GLushort item_index, GLushort stack_index, mat4 model, vec3 pos)
{
	bool ok = true;
	if (item_index > this->items.size() - 1)
	{
		ok = false;
	}
	if (stack_index > this->items[item_index]->stack.size() - 1)
	{
		ok = false;
	}
	if (!ok)
	{
		std::cout << "ERROR::ENGINE::CHANGING STACK" << std::endl
			<< "Item index: " << item_index << std::endl
			<< "Stack index: " << stack_index << std::endl
			<< "Size of items: " << this->items.size() << std::endl
			<< "Size of stack: " << this->items.size() << std::endl;
		exit(-1);
	}
	this->items[item_index]->stack[stack_index] = { model, pos };
}



void Engine::addMark(GLushort index)
{
	this->marked_items.push_back(this->items[index]);
}