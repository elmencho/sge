#include "Manager.hpp"

void Manager::addUniform(Object* object, Manager::Uniform uniform)
{
    object->propert.model = uniform.model;
    object->propert.camera = uniform.camera;
    object->propert.position = uniform.position;
}
void Manager::addUniform(Object* object, Manager::UniformWC uniform)
{
    object->propert.model = uniform.model;
    object->propert.position = uniform.position;
}
void Manager::setUniform(Object* object, Shader* shader)
{
    mat4 model = object->propert.model;
    mat3 modelNormal = transpose(inverse(model));

    shader->setMat4("view", object->propert.camera->view);
    shader->setVec3("viewPos", object->propert.camera->Pos);
    shader->setMat4("projection", object->propert.camera->projection);

    shader->setMat4("model", model);
    shader->setMat3("modelNormal", modelNormal);

    if (object->light != nullptr)
    {
        shader->setVec3("light.position", object->light->propert.position);
        shader->setVec3("light.specular", vec3(1.0f));
        shader->setVec3("light.diffuse", vec3(0.8f));
    }
}
void Manager::draw(Object* object)
{
    for (GLuint i = 0; i < object->model->texture_cache.size(); i++)
    {
        object->shader->setInt(object->model->texture_cache[i].type, i);
    }
    object->model->Draw();
}
void Manager::drawMarked(Object* object)
{
    object->model->Draw();
}
void Manager::loadModel(Object* object, std::string mdlPath)
{
    std::ifstream fin;
    fin.open(mdlPath);
    if (!fin.is_open())
    {
	    std::cout << "ERROR::MODEL::3D::NO SUCH FILE\n"
	        << mdlPath << std::endl;;
	    exit(EXIT_FAILURE);
    }
    fin.close();
    object->model = new Model(mdlPath);
}
Manager::Object* Manager::loadObject(std::string mdlPath, Shader shader, const Object* light)
{
    Object* object = new Object;

    Manager::loadModel(object, mdlPath);
    *(object->shader) = shader;

    if (light == nullptr)
    {
        object->light = nullptr;
        return object;
    }
    object->light = light;
    return object;
}
Manager::Object* Manager::loadObject(std::string mdlPath, Shader* shader, const Object* light)
{
    Object* object = new Object;

    Manager::loadModel(object, mdlPath);
    object->shader = shader;

    if (light == nullptr)
    {
        object->light = nullptr;
        return object;
    }
    object->light = light;
    return object;
}
Manager::Object::Object()
{
    model  = nullptr;
    shader = nullptr;
    light = nullptr;
}
Manager::Object::~Object()
{
    delete model;
    delete shader;
}
