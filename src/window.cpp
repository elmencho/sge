#include "Window.hpp"

GLFWwindow* initWindow(const WinLib impl) {
    switch (impl)
    {
        case GLFW:
            glfwInit();
            glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
            GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Simple Game Engine v0.0.1", NULL, NULL);
            glfwMakeContextCurrent(window);
            glfwSetCursorPosCallback(window, mouse_callback);
            glfwSetFramebufferSizeCallback(window, window_callback);
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            return window;
            break;
    }
}
void window_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}
void initGLES()
{
    gladLoadGLES2Loader((GLADloadproc)glfwGetProcAddress);

    glViewport(0, 0, WIDTH, HEIGHT);
    glClearColor(0.2f, 0.1f, 0.3f, 1.0f);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
}
