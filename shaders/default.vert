#version 310 es
layout(location = 0) in highp vec3 Vertex;
layout(location = 1) in highp vec3 inNormal;
layout(location = 2) in highp vec2 inTexel;

uniform mat4 view;
uniform mat4 model;
uniform mat4 projection;
uniform mat3 modelNormal;

out highp vec3 Position;
out highp vec3 Normal;
out highp vec2 Texel;

void main()
{
	Texel = inTexel;
	Normal = normalize(modelNormal * inNormal);
	Position = vec3(model * vec4(Vertex, 1.0));
	gl_Position = projection * view * vec4(Position, 1.0);
}
