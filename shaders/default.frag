#version 310 es
out highp vec4 FragColor;
in highp vec3 Position;
in highp vec3 Normal;
in highp vec2 Texel;

uniform highp vec3 viewPos;
uniform struct
{
    highp vec3 diffuse;
    highp vec3 position;
    highp vec3 specular;
} light;
uniform sampler2D diffTex;
uniform sampler2D specTex;

highp vec3 pointLight(highp vec3 diffColor, highp vec3 specColor)
{
    highp vec3 rayDir = normalize(light.position-Position);
    highp float rayCos = max(dot(Normal, rayDir), 0.0);
    highp vec3 diffuse = rayCos * light.diffuse * diffColor;

    highp vec3 viewDir = normalize(viewPos-Position);
    highp vec3 reflectDir = reflect(-rayDir, Normal);
    highp float viewCos = pow(max(dot(reflectDir, viewDir), 0.0), 128.0*specColor.r);
    highp vec3 specular = viewCos * light.specular * specColor;
    return diffuse + specular;
}
highp vec3 spotLight(highp vec3 diffColor, highp vec3 specColor)
{
    highp vec3 rayDir = normalize(light.position-Position);
    highp float rayCos = max(dot(Normal, rayDir), 0.0);
    highp vec3 diffuse = rayCos * light.diffuse * diffColor;

    highp vec3 viewDir = normalize(viewPos-Position);
    highp vec3 reflectDir = reflect(-rayDir, Normal);
    highp float viewCos = pow(max(dot(reflectDir, viewDir), 0.0), 128.0*specColor.r);
    highp vec3 specular = viewCos * light.specular * specColor;
    if (rayCos < cos(30.0))
    {
        return diffuse + specular;
    }
    return diffColor; 
}

void main()
{
    highp vec3 diffColor = texture(diffTex, Texel).rgb;
    highp vec3 specColor = texture(specTex, Texel).rgb;
    highp vec3 Color = pointLight(diffColor, specColor);
    FragColor = vec4(Color, 1.0);
}
