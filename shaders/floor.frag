#version 310 es
out highp vec4 FragColor;
in highp vec3 Position;
in highp vec3 Normal;

uniform highp vec3 viewPos;
uniform struct
{
    highp vec3 diffuse;
    highp vec3 position;
    highp vec3 specular;
} light;

highp vec3 Color = vec3(0.8, 0.9, 0.5);

highp vec3 pointLight()
{
    highp vec3 rayDir = normalize(light.position-Position);
    highp float rayCos = max(dot(Normal, rayDir), 0.0);
    highp vec3 diffuse = rayCos * light.diffuse * Color;

    highp vec3 viewDir = normalize(viewPos-Position);
    highp vec3 reflectDir = reflect(-rayDir, Normal);
    highp float viewCos = pow(max(dot(reflectDir, viewDir), 0.0), 32.0);
    highp vec3 specular = viewCos * light.specular * Color;
    return diffuse + specular;
}
void main()
{
    highp vec3 color = pointLight();
    FragColor = vec4(color, 1.0);
}
