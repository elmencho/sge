#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;

class Camera {
public:
    float sens;
    GLFWwindow* window;
    vec3 Pos, Front, Up;
    mat4 view, projection;

    Camera(GLFWwindow* window, vec3 Position, vec3 Front, float fov, float sensitivity);
    void rotate_callback(GLFWwindow* window, float x, float y);
    void move(float speed);
private:
    float lastx, lasty;
    float yaw = -90.0f;
    float pitch = 0.0f;
    bool firstCursorAppear = true;
    bool cursorEnabled = true;
    const vec3 globalUp = vec3(0.0f, 1.0f, 0.0f);
};
#endif // CAMERA_HPP
