#pragma once
#ifndef MANAGER_HPP
#define MANAGER_HPP

#include <string>
#include <vector>
#include <fstream>
#include <glm/gtc/matrix_transform.hpp>
#include "Shader.hpp"
#include "Camera.hpp"
#include "Model.hpp"

namespace Manager
{
struct Uniform
{
	glm::mat4 model;
	glm::vec3 position;
	Camera* camera = nullptr;
};
class Object
{
public:
	Manager::Uniform propert;
	Model* model;
	Shader* shader;
	const Object* light;

	Object();
	~Object();
};
struct UniformWC
{
	glm::mat4 model;
	glm::vec3 position;
};


void draw(Object*);
void drawMarked(Object*);
void setUniform(Object*, Shader*);
void loadModel(Object*, std::string);
void addUniform(Object*, Manager::Uniform);
void addUniform(Object*, Manager::UniformWC);
//void loadShader(Object*, std::string, std::string);

Object* loadObject(std::string, Shader, const Object* = nullptr);
Object* loadObject(std::string, Shader*, const Object* = nullptr);
}
#endif // MANAGER_HPP
