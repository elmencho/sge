#pragma once
#ifndef MODEL_HPP
#define MODEL_HPP

#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <glm/glm.hpp>
#include <vector>
#include <string>
#include <iostream>
#include "Mesh.hpp"

class Model {
public:
    std::vector<Mesh> meshes;
    std::vector<Texture> texture_cache;

    Model(std::string objPath);
    void Draw();
    ~Model();

private:
    std::string path;
    std::vector<aiString> texture_filenames;
    void getMeshes(aiNode* node, const aiScene* scene);
    Mesh createMesh(aiMesh* mesh, const aiScene* scene);
    void loadTextures(aiMaterial* material, aiTextureType aitype, const char* type);
};
#endif // MODEL_HPP
