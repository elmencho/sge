#pragma once
#ifndef ENGINE_HPP
#define ENGINE_HPP
#include <vector>
#include <string>
#include "Camera.hpp"
#include "Shader.hpp"
#include "Manager.hpp"

const std::string PREFIX = "";
const std::string DEFAULT_VERT = "shaders/default.vert";
const std::string DEFAULT_FRAG = "shaders/default.frag";
const std::string DEFAULT_FRAG_MARK = "shaders/mark.frag";
const std::string DEFAULT_FRAG_LIGHT = "shaders/lamp.frag";

struct Item
{
    Manager::Object* object = nullptr;
    std::vector<Manager::UniformWC> stack;
};

class Engine
{
public:
    Camera* camera;
    Manager::Object* terrain;
    std::vector<Item*> items;
    Shader* coreShader;
    Shader* markShader;
    Shader* coreLampShader;
    std::vector<Item*> marked_items;
    std::vector<Manager::Object*> lights;
    float Frame, lastFrame, deltaFrame;
    const enum OptionChange { OWN, NEW };
    const enum LightType { light_point, light_spot };

    Engine(GLFWwindow*);
    void init();
    void draw();

    void addItem(std::string);
    void changeItem(GLushort, OptionChange, glm::vec3 = glm::vec3(1.0f), glm::vec3 = glm::vec3(1.0f), glm::vec3 = glm::vec3(1.0f), GLfloat = 0.0f);

    void addLight(std::string, LightType);
    void changeLight(GLushort, OptionChange, glm::vec3 = glm::vec3(1.0f), glm::vec3 = glm::vec3(1.0f), glm::vec3 = glm::vec3(1.0f), GLfloat = 0.0f);

    void addStack(GLushort, size_t);
    void addStack(GLushort, glm::mat4, glm::vec3);
    void chgStack(GLushort item, GLushort element, glm::mat4, glm::vec3);

    void addMark(GLushort);
};

#endif // ENGINE_HPP
