#pragma once
#ifndef SHADER_HPP
#define SHADER_HPP

#include "glad/glad.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <glm/glm.hpp>

class Shader {
public:
    GLuint ID;
    Shader(std::string vertPath, std::string fragmPath);
    void use();
    ~Shader();

    void setVec3(std::string name, const float value[3]) const;
    void setMat4(std::string name, glm::mat4& value) const;
    void setMat3(std::string name, glm::mat3& value) const;
    void setVec3(std::string name, glm::vec3 value) const;
    void setFloat(std::string name, float value) const;
    void setBool(std::string name, bool value) const;
    void setInt(std::string name, int value) const;

private:
    void checkCompileErrors(GLuint shader, std::string type);
};
#endif // SHADER_HPP
