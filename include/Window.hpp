#pragma once
#ifndef WINDOW_HPP
#define WINDOW_HPP

#include "glad/glad.h"
#include <GLFW/glfw3.h>

static const short WIDTH = 1280;
static const short HEIGHT = 720;
enum WinLib {QT, GLFW};
void window_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow *window, double x, double y);
GLFWwindow* initWindow(const WinLib);
void initGLES();

#endif // WINDOW_HPP
