#pragma once
#ifndef MESH_HPP
#define MESH_HPP

#include "glad/glad.h"
#include "stb_image.hpp"
#include <glm/glm.hpp>
#include <iostream>
#include <string>
#include <vector>

GLuint loadTexture(const char* filename);
struct Vertex {
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 Texel;
};
struct Texture {
    const char* type = 0;
    std::string name;
    //GLubyte index;
    GLuint id = 0;
};

class Mesh {
public:
    GLuint VIBO[2], VAO;
    size_t indices_size;
    Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices);
    void Draw();
    ~Mesh();
private:
    void setup(std::vector<Vertex> vertices, std::vector<GLuint> indices);
};
#endif // MESH_HPP
